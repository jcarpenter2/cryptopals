#include <iostream>

#include "crypto.h"

int main(int argc, char *argv[]) {
  std::cout << pkcs7::pad(argv[2], std::stoi(argv[1])) << std::endl;
}
