#include <cstdlib>
#include <ctime>
#include <functional>
#include <iostream>
#include <limits>
#include <set>
#include <string>
#include <vector>

#include "crypto.h"
#include "read_hex.h"

struct SuffixDecoder {
  std::function<std::string(std::string)> encrypt_with_suffix;
  size_t block_length;

  std::unordered_map<std::string, char> create_dictionary(std::string decoded) {
    std::unordered_map<std::string, char> result;

    std::string prefix;
    if (decoded.size() >= block_length - 1) {
      prefix = decoded.substr(decoded.size() - (block_length - 1));
    }
    else {
      prefix = std::string(block_length - 1 - decoded.size(), 'A') + decoded;
    }
    for (int i = 0; i <= std::numeric_limits<unsigned char>::max(); i++) {
      char ch = static_cast<char>(i);
      result[encrypt_with_suffix(prefix + ch).substr(0, block_length)] = ch;
    }

    return result;
  }

  std::string decode() {
    std::string result;
    size_t expect_len = encrypt_with_suffix("").size();

    while (result.size() < expect_len) {
      std::cout << result.size() << std::endl;
      auto dict = create_dictionary(result);
      auto encrypted = encrypt_with_suffix(std::string((block_length - 1) - (result.size() % block_length), 'A'));
      result += dict[encrypted.substr(block_length * (result.size() / block_length), block_length)];
    }

    return result;
  }
};

int main(int argc, char *argv[]) {
  std::string str;
  std::string in;
  while (std::getline(std::cin, in)) {
    str += in;
  }
  str = from_base64(str);

  std::srand(std::time(nullptr));

  std::string key;
  key.resize(16);
  for (auto &ch : key) {
    ch = std::rand();
  }

  SuffixDecoder decoder {
    [&](std::string prefix) {
      return aes::encrypt_ecb(prefix + str, key, aes::KeyLength::Bits128);
    },
    16,
  };

  std::cout << decoder.decode() << std::endl;

  return 0;
}
