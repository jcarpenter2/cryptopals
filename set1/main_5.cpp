#include <iostream>
#include <string>

#include "crypto.h"
#include "read_hex.h"

int main(int argc, char *argv[]) {
  if (argc < 3) {
    std::cout << "Provide string and a key" << std::endl;
    return 1;
  }

  std::cout << to_hex(repeating_key_xor(argv[1], argv[2])) << std::endl;
}
