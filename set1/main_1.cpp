#include <iostream>

#include "read_hex.h"

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cout << "Provide hex string to convert to base 64" << std::endl;
    return 1;
  }
  std::cout << to_base64(from_hex(argv[1])) << std::endl;
}
