#include <iostream>
#include <set>
#include <string>

#include "crypto.h"
#include "frequency.h"
#include "read_hex.h"

int main(int argc, char *argv[]) {
  std::string str;

  std::multiset<Distance<std::string>> set;

  while (std::getline(std::cin, str)) {
    str = from_hex(str);

    auto single_character = best_single_character_xor(str);
    set.insert(Distance<std::string> {
      single_character.distance,
      single_character_xor(str, single_character.t),
    });
  }

  std::cout << set.begin()->t << std::endl;
}
