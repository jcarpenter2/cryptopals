#include <iostream>
#include <limits>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

#include "crypto.h"
#include "read_hex.h"

int main(int argc, char *argv[]) {
  std::vector<std::string> texts;
  std::string in;
  while (std::getline(std::cin, in)) {
    texts.push_back(from_hex(in));
  }

  using CountRepeats = Distance<std::string>;
  std::set<CountRepeats> repeats;
  for (auto text : texts) {
    std::unordered_map<std::string, size_t> text_repeats;
    for (size_t i = 0; i < text.size(); i += 16) {
      text_repeats[text.substr(i, 16)] += 1;
    }
    size_t count_repeats = 0;
    for (auto [key, value] : text_repeats) {
      count_repeats += value - 1;
    }
    repeats.insert(CountRepeats {
      -(double)count_repeats,
      text,
    });
  }

  std::cout << repeats.begin()->distance << std::endl;
  std::cout << to_hex(repeats.begin()->t) << std::endl;

  return 0;
}
