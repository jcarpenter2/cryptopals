#include <iostream>

#include "read_hex.h"

int main(int argc, char *argv[]) {
  if (argc < 3) {
    std::cout << "Provide hex strings to xor" << std::endl;
    return 1;
  }

  std::string str_1 = from_hex(argv[1]);
  std::string str_2 = from_hex(argv[2]);
  if (str_1.size() != str_2.size()) {
    std::cout << "Strings were different lengths" << std::endl;
    return 1;
  }

  std::string str_3;
  for (size_t i = 0; i < str_1.size(); i++) {
    str_3.push_back(str_1[i] ^ str_2[i]);
  }

  std::cout << to_hex(str_3) << std::endl;
}
