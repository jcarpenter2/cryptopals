#include <iostream>
#include <limits>
#include <set>
#include <string>
#include <vector>

#include "crypto.h"
#include "read_hex.h"

int main(int argc, char *argv[]) {
  std::string str;
  std::string in;
  while (std::getline(std::cin, in)) {
    str += in;
  }
  str = from_base64(str);

  std::multiset<Distance<int>> set;

  for (int i = 2; i < 40; i++) {
    double distance = 0;
    for (int j = 0; j < str.size() / i - 1; j++) {
      auto str1 = std::string_view(str).substr(j * i, i);
      auto str2 = std::string_view(str).substr((j + 1) * i, i);
      distance += hamming_distance(str1, str2);
    }
    distance /= (double)(str.size() - str.size() % i) / str.size();

    set.insert(Distance<int> {
      distance,
      i,
    });
  }

  auto best_length = set.begin()->t;

  auto t_strs = transpose(str, best_length);
  std::string key;
  for (auto &t_str : t_strs) {
    key += best_single_character_xor(t_str).t;
  }

  std::cout << key << std::endl;
  std::cout << repeating_key_xor(str, key) << std::endl;
}
