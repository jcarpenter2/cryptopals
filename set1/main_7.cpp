#include <iostream>
#include <limits>
#include <set>
#include <string>
#include <vector>

#include "crypto.h"
#include "read_hex.h"

int main(int argc, char *argv[]) {
  std::string str;
  std::string in;
  while (std::getline(std::cin, in)) {
    str += in;
  }
  str = from_base64(str);

  std::cout << aes::decrypt_ecb(str, "YELLOW SUBMARINE", aes::KeyLength::Bits128) << std::endl;

  return 0;
}
