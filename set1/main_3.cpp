#include <iostream>
#include <string>

#include "crypto.h"
#include "read_hex.h"

int main(int argc, char *argv[]) {
  if (argc < 2) {
    std::cout << "Provide string to decrypt" << std::endl;
    return 1;
  }

  std::string str = from_hex(argv[1]);
  auto best_char = best_single_character_xor(str);
  std::cout << single_character_xor(str, best_char.t) << std::endl;
}
