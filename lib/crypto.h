#pragma once

#include <array>
#include <limits>
#include <string>
#include <string_view>
#include <vector>

template<typename T>
struct Distance {
  double distance = std::numeric_limits<double>::infinity();
  T t;

  bool operator<(Distance const& op2) const {
    return distance < op2.distance;
  }
  bool operator>(Distance const& op2) const {
    return distance > op2.distance;
  }
};

std::vector<std::string>
transpose(std::string str, size_t len);

int
hamming_distance(char a, char b);

int
hamming_distance(std::string_view a, std::string_view b);

std::string
single_character_xor(std::string str, char key);

std::string
repeating_key_xor(std::string str, std::string key);

Distance<char>
best_single_character_xor(std::string str);

namespace pkcs7 {
std::string pad(std::string str, size_t bytes);
std::string unpad(std::string str, size_t bytes);
}

namespace aes {

enum class KeyLength {
  Bits128,
  Bits192,
  Bits256,
};

size_t
number_rounds(KeyLength length);

size_t
key_length(KeyLength length);

size_t
key_schedule_size(KeyLength length);

std::string
expand_key(std::string key, KeyLength length);

struct State {
  std::array<unsigned char, 16> matrix;
  unsigned char& elem(int row, int column);
  unsigned char& elem(int pos);
};

std::string
encrypt_16(std::string input, std::string const& key, KeyLength length);

std::string
decrypt_16(std::string input, std::string const& key, KeyLength length);

std::string
encrypt_ecb(std::string input, std::string key, KeyLength length);

std::string
decrypt_ecb(std::string input, std::string key, KeyLength length);

std::string
encrypt_cbc(std::string input, std::string key, KeyLength length, std::string iv);

std::string
decrypt_cbc(std::string input, std::string key, KeyLength length, std::string iv);

}
