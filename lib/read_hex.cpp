#include <iostream>
#include <stdexcept>

#include "read_hex.h"

char hex_to_nibble(char hex) {
  if (hex >= '0' && hex <= '9') {
    return hex - '0';
  }
  if (hex >= 'a' && hex <= 'f') {
    return 10 + hex - 'a';
  }
  throw std::runtime_error("hex_to_nible: invalid character");
}

std::string
from_hex(std::string hex) {
  if (hex.size() % 2) {
    throw std::runtime_error("read_hex: Hex string contained an odd number of characters");
  }

  std::string result;
  result.resize(hex.size() / 2);

  for (size_t i = 0; i < hex.size() / 2; i++) {
    result[i] = 16 * hex_to_nibble(hex[2 * i]) + hex_to_nibble(hex[2 * i + 1]);
  }

  return result;
}

std::string
to_hex(std::string str) {
  std::string result;
  result.reserve(str.size() * 2);

  std::string alphabet = "0123456789abcdef";
  for (auto ch : str) {
    result.push_back(alphabet[(ch >> 4) & 0x0f]);
    result.push_back(alphabet[ch & 0x0f]);
  }

  return result;
}

std::string
to_base64(std::string str) {
  std::string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

  std::string result;
  result.reserve(str.size() * 4/3 + 3); // more or less big enough

  int stage = 0;
  int idx = 0;
  for (auto ch : str) {
    switch (stage) {
    case 0:
      idx = ch >> 2;
      result.push_back(alphabet[idx]);
      idx = (ch & 0x03) << 4;

      stage = 1;
      break;
    case 1:
      idx |= (ch & 0xF0) >> 4;
      result.push_back(alphabet[idx]);
      idx = (ch & 0x0F) << 2;

      stage = 2;
      break;
    case 2:
      idx |= (ch & 0xC0) >> 6;
      result.push_back(alphabet[idx]);
      result.push_back(alphabet[ch & 0x3F]);

      stage = 0;
      break;
    }
  }

  switch (stage) {
  case 0:
    break;
  case 1:
    result.push_back(alphabet[idx]);
    result += "==";
    break;
  case 2:
    result.push_back(alphabet[idx]);
    result += "=";
    break;
  }

  return result;
}

char
base64_char_value(char ch) {
  if (ch >= 'A' && ch <= 'Z') {
    return ch - 'A';
  }
  if (ch >= 'a' && ch <= 'z') {
    return 26 + ch - 'a';
  }
  if (ch >= '0' && ch <= '9') {
    return 52 + ch - '0';
  }
  if (ch == '+') {
    return 62;
  }
  if (ch == '/') {
    return 63;
  }
  throw std::runtime_error("base64_char_value: invalid character");
}

std::string
from_base64(std::string str) {
  std::string result;
  result.reserve(str.size() * 3/4 + 1); // more or less big enough

  char byte;

  int stage = 0;
  for (auto ch : str) {
    if (ch == '=') {
      break;
    }
    switch (stage) {
    case 0:
      byte = base64_char_value(ch) << 2;
      stage = 1;
      break;
    case 1: {
      auto ch_value = base64_char_value(ch);
      byte |= ch_value >> 4;
      result.push_back(byte);
      byte = (ch_value & 0x0f) << 4;
      stage = 2;
      break;
    }
    case 2: {
      auto ch_value = base64_char_value(ch);
      byte |= ch_value >> 2;
      result.push_back(byte);
      byte = (ch_value & 0x7) << 6;
      stage = 3;
      break;
    }
    case 3:
      byte |= base64_char_value(ch);
      result.push_back(byte);
      stage = 0;
      break;
    }
  }

  return result;
}
