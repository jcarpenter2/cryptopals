#pragma once

#include <string>

std::string
from_hex(std::string hex);

std::string
to_hex(std::string hex);

std::string
to_base64(std::string str);

std::string
from_base64(std::string str);
