#include <cstring>
#include <limits>
#include <stdexcept>

#include "crypto.h"
#include "frequency.h"

std::vector<std::string>
transpose(std::string str, size_t len) {
  std::vector<std::string> result(len);

  for (size_t i = 0; i < str.size(); i++) {
    result[i % len] += str[i];
  }

  return result;
}

int
hamming_distance(char a, char b) {
  char c = a ^ b;

  int distance = 0;
  for (int i = 0; i < 8; i++) {
    if (c & (1 << i)) {
      distance++;
    }
  }
  return distance;
}

int
hamming_distance(std::string_view a, std::string_view b) {
  if (a.size() != b.size()) {
    throw std::runtime_error("hamming_distance: Strings of unequal sizes");
  }

  int distance = 0;
  for (size_t i = 0; i < a.size(); i++) {
    distance += hamming_distance(a[i], b[i]);
  }
  return distance;
}

std::string
single_character_xor(std::string str, char key) {
  for (auto &ch : str) {
    ch ^= key;
  }
  return str;
}

std::string
repeating_key_xor(std::string str, std::string key) {
  for (size_t i = 0; i < str.size(); i++) {
    str[i] ^= key[i % key.size()];
  }
  return str;
}

Distance<char>
best_single_character_xor(std::string str) {
  Distance<char> result {
    std::numeric_limits<double>::infinity(),
    0,
  };

  for (int i = 0; i < 255; i++) {
    std::string str2 = single_character_xor(str, i);

    double distance = english_distance(str2);
    if (distance < result.distance) {
      result.distance = distance;
      result.t = i;
    }
  }

  return result;
}


namespace pkcs7 {

std::string pad(std::string str, size_t bytes) {
  unsigned char pad_len = bytes - str.size() % bytes;
  return str + std::string(pad_len, pad_len);
}

}


namespace aes {

size_t
number_rounds(KeyLength length) {
  switch (length) {
  case KeyLength::Bits128:
    return 10;
  case KeyLength::Bits192:
    return 12;
  case KeyLength::Bits256:
    return 14;
  }
  exit(1);
}

size_t
key_length(KeyLength length) {
  switch (length) {
  case KeyLength::Bits128:
    return 16;
  case KeyLength::Bits192:
    return 24;
  case KeyLength::Bits256:
    return 32;
  }
  exit(1);
}

size_t
key_schedule_size(KeyLength length) {
  return 16 * (number_rounds(length) + 1);
}

uint32_t
rot_word(uint32_t w) {
  return (w << 8) + ((w & 0xff000000) >> 24);
}

unsigned char
sub_byte(unsigned char ch) {
  std::vector<unsigned char> sub_table {
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
  };
  return sub_table[ch];
}

unsigned char
inv_sub_byte(unsigned char ch) {
  std::vector<unsigned char> sub_table {
    0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
    0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
    0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
    0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
    0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
    0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
    0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
    0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
    0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
    0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
    0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
    0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
    0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
    0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
    0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
    0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d,
  };
  return sub_table[ch];
}

uint32_t
sub_word(uint32_t w) {
  return
    sub_byte(w >> 24) << 24 |
    sub_byte((w >> 16) & 0xff) << 16 |
    sub_byte((w >> 8) & 0xff) << 8 |
    sub_byte(w & 0xff);
}

uint32_t
inv_sub_word(uint32_t w) {
  return
    inv_sub_byte(w >> 24) << 24 |
    inv_sub_byte((w >> 16) & 0xff) << 16 |
    inv_sub_byte((w >> 8) & 0xff) << 8 |
    inv_sub_byte(w & 0xff);
}

std::string
expand_key(std::string key, KeyLength length) {
  size_t Nk4 = key_length(length);

  while (key.size() < Nk4) {
    key = key + key;
  }
  key.resize(key_schedule_size(length));

  std::vector<uint32_t> r_con {
    0x0,
    0x01000000,
    0x02000000,
    0x04000000,
    0x08000000,
    0x10000000,
    0x20000000,
    0x40000000,
    0x80000000,
    0x1b000000,
    0x36000000,
  };

  for (size_t i = Nk4 / 4; i < key_schedule_size(length) / 4; i++) {
    uint32_t temp =
      ((key[4*i-4] << 24) & 0xff000000) +
      ((key[4*i-3] << 16) & 0xff0000) +
      ((key[4*i-2] << 8) & 0xff00) +
      (key[(4*i-1)] & 0xff);
    if (i % (Nk4 / 4) == 0) {
      temp = rot_word(temp);
      temp = sub_word(temp);
      temp ^= r_con[i / (Nk4 / 4)];
    }
    else if ((Nk4 / 4) > 6 && (4 == i % (Nk4 / 4))) {
      temp = sub_word(temp);
    }
    key[4*i] = (temp >> 24) ^ key[4*i-Nk4];
    key[4*i + 1] = ((temp >> 16) & 0xff) ^ key[4*i-Nk4+1];
    key[4*i + 2] = ((temp >> 8) & 0xff) ^ key[4*i-Nk4+2];
    key[4*i + 3] = (temp & 0xff) ^ key[4*i-Nk4+3];
  }

  return key;
}

unsigned char& State::elem(int row, int column) {
  return matrix[4 * column + row];
}

unsigned char& State::elem(int pos) {
  return elem(pos % 4, pos / 4);
}

void AddRoundKey(State &state, std::string_view str) {
  for (auto i = 0; i < 16; i++) {
    state.elem(i) ^= str[i];
  }
}

void SubBytes(State &state) {
  for (auto &ch : state.matrix) {
    ch = sub_byte(ch);
  }
}

void InvSubBytes(State &state) {
  for (auto &ch : state.matrix) {
    ch = inv_sub_byte(ch);
  }
}

void ShiftRows(State &state) {
  char ch = state.elem(1, 0);
  state.elem(1, 0) = state.elem(1, 1);
  state.elem(1, 1) = state.elem(1, 2);
  state.elem(1, 2) = state.elem(1, 3);
  state.elem(1, 3) = ch;

  ch = state.elem(2, 0);
  state.elem(2, 0) = state.elem(2, 2);
  state.elem(2, 2) = ch;
  ch = state.elem(2, 1);
  state.elem(2, 1) = state.elem(2, 3);
  state.elem(2, 3) = ch;

  ch = state.elem(3, 3);
  state.elem(3, 3) = state.elem(3, 2);
  state.elem(3, 2) = state.elem(3, 1);
  state.elem(3, 1) = state.elem(3, 0);
  state.elem(3, 0) = ch;
}

void InvShiftRows(State &state) {
  char ch = state.elem(1, 3);
  state.elem(1, 3) = state.elem(1, 2);
  state.elem(1, 2) = state.elem(1, 1);
  state.elem(1, 1) = state.elem(1, 0);
  state.elem(1, 0) = ch;

  ch = state.elem(2, 0);
  state.elem(2, 0) = state.elem(2, 2);
  state.elem(2, 2) = ch;
  ch = state.elem(2, 1);
  state.elem(2, 1) = state.elem(2, 3);
  state.elem(2, 3) = ch;

  ch = state.elem(3, 0);
  state.elem(3, 0) = state.elem(3, 1);
  state.elem(3, 1) = state.elem(3, 2);
  state.elem(3, 2) = state.elem(3, 3);
  state.elem(3, 3) = ch;
}

int poly_mult(char a, char b) {
  int sum = 0;
  for (int i = 0; i < 8; i++) {
    sum ^= a * (b & (1 << i));
  }
  for (int i = 8; i >= 0; i--) {
    if ((256 << i) <= sum) {
      sum ^= (283 << i);
    }
  }
  return sum;
}

void MixColumns(State &state) {
  State out;
  for (size_t column = 0; column < 4; column++) {
    out.elem(0, column) =
      poly_mult(2, state.elem(0, column)) ^
      poly_mult(3, state.elem(1, column)) ^
      poly_mult(1, state.elem(2, column)) ^
      poly_mult(1, state.elem(3, column));

    out.elem(1, column) =
      poly_mult(1, state.elem(0, column)) ^
      poly_mult(2, state.elem(1, column)) ^
      poly_mult(3, state.elem(2, column)) ^
      poly_mult(1, state.elem(3, column));
    
    out.elem(2, column) =
      poly_mult(1, state.elem(0, column)) ^
      poly_mult(1, state.elem(1, column)) ^
      poly_mult(2, state.elem(2, column)) ^
      poly_mult(3, state.elem(3, column));
    
    out.elem(3, column) =
      poly_mult(3, state.elem(0, column)) ^
      poly_mult(1, state.elem(1, column)) ^
      poly_mult(1, state.elem(2, column)) ^
      poly_mult(2, state.elem(3, column));
  }
  state = out;
}

void InvMixColumns(State &state) {
  State out;
  for (size_t column = 0; column < 4; column++) {
    out.elem(0, column) =
      poly_mult(0xe, state.elem(0, column)) ^
      poly_mult(0xb, state.elem(1, column)) ^
      poly_mult(0xd, state.elem(2, column)) ^
      poly_mult(0x9, state.elem(3, column));

    out.elem(1, column) =
      poly_mult(0x9, state.elem(0, column)) ^
      poly_mult(0xe, state.elem(1, column)) ^
      poly_mult(0xb, state.elem(2, column)) ^
      poly_mult(0xd, state.elem(3, column));
    
    out.elem(2, column) =
      poly_mult(0xd, state.elem(0, column)) ^
      poly_mult(0x9, state.elem(1, column)) ^
      poly_mult(0xe, state.elem(2, column)) ^
      poly_mult(0xb, state.elem(3, column));
    
    out.elem(3, column) =
      poly_mult(0xb, state.elem(0, column)) ^
      poly_mult(0xd, state.elem(1, column)) ^
      poly_mult(0x9, state.elem(2, column)) ^
      poly_mult(0xe, state.elem(3, column));
  }
  state = out;
}

std::string
encrypt_16(std::string input, std::string const& key, KeyLength length) {
  if (input.size() != 16) {
    throw std::runtime_error("encrypt_16 not passed 16-byte input");
  }
  if (key.size() != key_schedule_size(length)) {
    throw std::runtime_error("encrypt_16 not passed expanded key");
  }

  State state;
  memcpy(state.matrix.data(), input.data(), 16);

  AddRoundKey(state, key.substr(0, 16));

  for (size_t i = 1; i < number_rounds(length); i++) {
    SubBytes(state);
    ShiftRows(state);
    MixColumns(state);
    AddRoundKey(state, key.substr(16 * i, 16));
  }

  SubBytes(state);
  ShiftRows(state);
  AddRoundKey(state, key.substr(16 * number_rounds(length), 16));

  std::string result;
  result.resize(16);
  memcpy(result.data(), state.matrix.data(), 16);
  return result;
}

std::string
decrypt_16(std::string input, std::string const& key, KeyLength length) {
  if (input.size() != 16) {
    throw std::runtime_error("encrypt_16 not passed 16-byte input");
  }
  if (key.size() != key_schedule_size(length)) {
    throw std::runtime_error("encrypt_16 not passed expanded key");
  }

  State state;
  memcpy(state.matrix.data(), input.data(), 16);

  AddRoundKey(state, key.substr(16 * number_rounds(length), 16));

  for (size_t i = number_rounds(length) - 1; i >= 1; i--) {
    InvShiftRows(state);
    InvSubBytes(state);
    AddRoundKey(state, key.substr(16 * i, 16));
    InvMixColumns(state);
  }

  InvShiftRows(state);
  InvSubBytes(state);
  AddRoundKey(state, key.substr(0, 16));

  std::string result;
  result.resize(16);
  memcpy(result.data(), state.matrix.data(), 16);
  return result;
}

std::string
encrypt_ecb(std::string input, std::string key, KeyLength length) {
  auto size = input.size();
  if (auto x = size % 16) {
    input.resize(size + 16 - x);
  }

  std::string output;

  key = expand_key(key, length);

  for (size_t i = 0; i < input.size(); i += 16) {
    output += encrypt_16(input.substr(i, 16), key, length);
  }

  output.resize(size);
  return output;
}

std::string
decrypt_ecb(std::string input, std::string key, KeyLength length) {
  auto size = input.size();
  if (auto x = size % 16) {
    input.resize(size + 16 - x);
  }

  std::string output;

  key = expand_key(key, length);

  for (size_t i = 0; i < input.size(); i += 16) {
    output += decrypt_16(input.substr(i, 16), key, length);
  }

  output.resize(size);
  return output;
}

std::string
encrypt_cbc(std::string input, std::string key, KeyLength length, std::string iv) {
  auto size = input.size();
  if (auto x = size % 16) {
    input.resize(size + 16 - x);
  }
  iv.resize(16);

  std::string output;

  key = expand_key(key, length);

  for (size_t i = 0; i < input.size(); i += 16) {
    auto in = repeating_key_xor(input.substr(i, 16), iv);
    iv = encrypt_16(in, key, length);
    output += iv;
  }

  output.resize(size);
  return output;
}

std::string
decrypt_cbc(std::string input, std::string key, KeyLength length, std::string iv) {
  auto size = input.size();
  if (auto x = size % 16) {
    input.resize(size + 16 - x);
  }
  iv.resize(16);

  std::string output;

  key = expand_key(key, length);

  for (size_t i = 0; i < input.size(); i += 16) {
    auto in = input.substr(i, 16);
    output += repeating_key_xor(decrypt_16(in, key, length), iv);
    iv = in;
  }

  output.resize(size);
  return output;
}

}
