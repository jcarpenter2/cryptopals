#pragma once

#include <string>
#include <unordered_map>

std::unordered_map<char, double> const&
frequency_table();

double
english_distance(std::string str);
