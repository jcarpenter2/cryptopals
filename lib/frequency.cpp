#include <iostream>
#include <limits>

#include "frequency.h"

std::unordered_map<char, double> const&
frequency_table() {
  static std::unordered_map<char, double> frequencies {
    {'E', 0.1202},
    {'T', 0.0910},
    {'A', 0.0812},
    {'O', 0.0768},
    {'I', 0.0731},
    {'N', 0.0695},
    {'S', 0.0628},
    {'R', 0.0602},
    {'H', 0.0592},
    {'D', 0.0432},
    {'L', 0.0398},
    {'U', 0.0288},
    {'C', 0.0271},
    {'M', 0.0261},
    {'F', 0.0230},
    {'Y', 0.0211},
    {'W', 0.0209},
    {'G', 0.0203},
    {'P', 0.0182},
    {'B', 0.0149},
    {'V', 0.0111},
    {'K', 0.0069},
    {'X', 0.0017},
    {'Q', 0.0011},
    {'J', 0.0010},
    {'Z', 0.0007},
  };

  return frequencies;
}

double
english_distance(std::string str) {
  int total = 0;
  std::unordered_map<char, double> counts;

  for (auto &ch : str) {
    ch = std::toupper(ch);
  }

  for (auto ch : str) {
    counts[ch] += 1;
    total += 1;
  }

  for (auto &[ch, count] : counts) {
    count /= total;
  }

  double distance = 0;
  auto const& frequencies = frequency_table();
  for (auto &[ch, freq] : counts) {
    if (ch < 32 && ch != '\n' && ch != '\r') {
      return std::numeric_limits<double>::infinity();
    }
    else if (!frequencies.count(ch)) {
      distance += freq * str.size();
    }
    else {
      distance += std::abs(frequencies.at(ch) - freq);
    }
  }

  return distance;
}
